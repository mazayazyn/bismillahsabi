from .apps import Story7Config
from .views import accordion
from django.urls import reverse, resolve
from django.test import LiveServerTestCase, TestCase, Client
from django.apps import apps

class Stroy7Test(TestCase) :
    # App Test
    def test_apps_is_true(self) :
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')

    #Set Up
    def setUp(self):
        self.client = Client()
        self.accordion = reverse("accordion")

    #Check status code
    def test_status_code(self): 
        response = self.client.get(self.accordion)
        self.assertEqual(response.status_code, 200)

    #Template test
    def test_template_used_accordion(self): 
        response = self.client.get(self.accordion)
        self.assertTemplateUsed(response, 'dropdown.html')

    #Test Function accordion
    def test_function_used_by_accordion(self) :
        found = resolve(self.accordion)
        self.assertEqual(found.func, accordion)

