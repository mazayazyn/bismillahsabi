from django.db import models

# Create your models here.
class Schedule(models.Model):
    mata_kuliah = models.CharField(max_length=50, blank=False)
    dosen_pengajar = models.CharField(max_length=50, blank=False)
    jumlah_SKS = models.CharField(max_length=50, blank=False)
    deskripsi_mata_kuliah= models.CharField(max_length=50, blank=False)
    tahun_ajaran = models.CharField(max_length=50, blank=False)
    ruang_kelas = models.CharField(max_length=50, blank=False)