from django import forms

class ScheduleForm(forms.Form):
    mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'MataKuliah',
        'required': True,
    }))

    dosen_pengajar = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'DosenPengajar',
        'required': True,
    }))

    jumlah_SKS = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'JumlahSKS',
        'required': True,
    }))

    deskripsi_mata_kuliah = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'DeskripsiMataKuliah',
        'required': True,
    }))

    tahun_ajaran = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'TahunAjaran',
        'required': True,
    }))

    ruang_kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'RuangKelas',
        'required': True,
    }))