from django.urls import path
from .views import schedule_show, schedule_remove, schedule_form, schedule_detail

urlpatterns = [
   path('', schedule_form, name = 'schedule'),
   path('result/', schedule_show, name = 'scheduleresult'),
   path('result/detail/<int:id>/', schedule_detail, name = 'detailschedule'),
   path('result/<int:id>/',schedule_remove, name = 'scheduleremove'),
]