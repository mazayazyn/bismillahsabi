from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm

# Create your views here.
def schedule_form(request):
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            # print("test")
            schedule = Schedule()
            schedule.mata_kuliah = form.cleaned_data['mata_kuliah']
            schedule.dosen_pengajar = form.cleaned_data['dosen_pengajar']
            schedule.jumlah_SKS = form.cleaned_data['jumlah_SKS']
            schedule.deskripsi_mata_kuliah = form.cleaned_data['deskripsi_mata_kuliah']
            schedule.tahun_ajaran = form.cleaned_data['tahun_ajaran']
            schedule.ruang_kelas = form.cleaned_data['ruang_kelas']
            schedule.save()
            return redirect('result/')
        return render(request,'story5/schedule.html',{'form' : form})
    form = ScheduleForm()
    return render(request,'story5/schedule.html',{'form' : form})


def schedule_show(request):
    schedule = Schedule.objects.all().order_by('dosen_pengajar').order_by('mata_kuliah')
    response = {
        'schedule':schedule, 
    }
    return render(request,'story5/scheduleresult.html',response)

def schedule_remove(request, id):
    Schedule.objects.filter(id=id).delete()
    schedule = Schedule.objects.all().order_by('dosen_pengajar').order_by('mata_kuliah')
    response = {
        'schedule':schedule, 
    }
    return render(request, 'story5/scheduleresult.html',response)

def schedule_detail(request, id):
    schedule = Schedule.objects.get(id=id)
    response = {
        'schedule':schedule, 
    }
    return render(request, 'story5/detailschedule.html',response)