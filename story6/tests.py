from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from story6.models import Kegiatan, PesertaKegiatan
from . import views
# from . import models
# from .views import index

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response=Client().get('/story6/')
        self.assertEqual(response.status_code,200)

    def test_template_used(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/kegiatan.html')

    def test_func_page(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, views.index)

    def test_story6_save_kegiatan_a_POST_request(self):
        Client().post('/story6/', data={'nama_kegiatan': 'Unit Test'})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 1)

    def test_story6_save_peserta_a_POST_request(self):
        Kegiatan.objects.create(nama='Unit Test2')
        Client().post('/story6/', data={'nama_peserta':'ABC', 'id_kegiatan':1})
        jumlah = PesertaKegiatan.objects.filter(nama='ABC').count()
        self.assertEqual(jumlah, 1)

    def test_story6_save_a_POST_request_2(self):
        Kegiatan.objects.create(nama='Test')
        response = Client().post('/story6/', data={'nama_peserta':'ABC', 'id_kegiatan':1})
        self.assertEqual(response.status_code,200)

    def test_story6_model_kegiatan(self):
        Kegiatan.objects.create(nama='Test Kegiatan')
        kegiatan = Kegiatan.objects.get(nama='Test Kegiatan')
        self.assertEqual(str(kegiatan), 'Test Kegiatan')

    def test_story6_model_create(self):
        Kegiatan.objects.create(nama='Demo TK1')
        jumlah = Kegiatan.objects.all().count()
        self.assertEqual(jumlah,1)

    def test_story6_kegiatan_form_invalid(self):
        Client().post('/story6/', data={})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 0)

    def test_story6_delete_POST_request(self):
        Kegiatan.objects.create(nama='Test')
        idpk = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=idpk)
        PesertaKegiatan.objects.create(nama='AzzaTest', kegiatan=kegiatan)
        response = Client().post('/story6/', data={'delete-peserta':1})
        self.assertEqual(response.status_code,200)
    
    def test_story6_bad_request(self):
        response = Client().post('/story6/', data={'nama_peserta':'ABC', 'id_kegiatan':99})
        self.assertEqual(response.status_code, 400)

    def test_story6_delete_POST_bad_request(self):
        response = Client().post('/story6/', data={'delete-peserta':99})
        self.assertEqual(response.status_code,400)

    def test_story6_model_create2(self):
        Kegiatan.objects.create(nama='Demo TK1')
        kegiatan = Kegiatan.objects.get(nama='Demo TK1')
        PesertaKegiatan.objects.create(nama='Azza', kegiatan=kegiatan)
        jumlah = PesertaKegiatan.objects.all().count()
        self.assertEqual(jumlah,1)
    
    def test_story6_model_relational(self):
        Kegiatan.objects.create(nama='Demo TK1')
        idpk = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=idpk)
        PesertaKegiatan.objects.create(nama='Azza', kegiatan=kegiatan)
        jumlah = PesertaKegiatan.objects.filter(kegiatan=kegiatan).count()
        self.assertEqual(jumlah,1)
    
    def test_story6_model_name2(self):
        Kegiatan.objects.create(nama='Test')
        idpk = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=idpk)
        PesertaKegiatan.objects.create(nama='AzzaTest', kegiatan=kegiatan)
        peserta = PesertaKegiatan.objects.get(kegiatan=kegiatan)
        self.assertEqual(str(peserta),'AzzaTest')





 

