from django.shortcuts import render
from . import forms
from . import models
# from .models import Kegiatan, PesertaKegiatan
# from .forms import KegiatanForm, PesertaForm
from django.http import HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt
def index(request):
    if request.method == 'POST':
        if 'nama_kegiatan' in request.POST:
            formkegiatan = forms.KegiatanForm(request.POST)
            if formkegiatan.is_valid():
                data = formkegiatan.cleaned_data
                data_input = models.Kegiatan()
                data_input.nama = data['nama_kegiatan']
                data_input.save()
        elif 'nama_peserta' in request.POST and 'id_kegiatan' in request.POST:
            formpeserta = forms.PesertaForm(request.POST)
            if formpeserta.is_valid():
                try:
                    kegiatan = models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])                
                    data = formpeserta.cleaned_data
                    data_input = models.PesertaKegiatan()
                    data_input.nama = data['nama_peserta']
                    data_input.kegiatan = kegiatan
                    data_input.save()
                except:
                    return HttpResponseBadRequest("Bad Request")
        elif 'delete-peserta' in request.POST:
            try:
                models.PesertaKegiatan.objects.get(id=request.POST['delete-peserta']).delete()
            except:
                return HttpResponseBadRequest("Bad Request")
    list_kegiatan = models.Kegiatan.objects.all()
    data_kegiatan_lengkap = []
    for kegiatan in list_kegiatan:
        list_peserta = models.PesertaKegiatan.objects.filter(kegiatan = kegiatan)
        data_peserta =[]
        for peserta in list_peserta:
            data_peserta.append(peserta)
        data_kegiatan_lengkap.append((kegiatan,data_peserta))
    return render(request, 'story6/kegiatan.html', {'formkegiatan':forms.KegiatanForm, 'formpeserta':forms.PesertaForm, 'data_kegiatan':data_kegiatan_lengkap})