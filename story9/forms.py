from django import forms

class formAja(forms.Form):
    user_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'User Name',
        'type' : 'text',
        'required' : True
    }))
    Password = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Password',
        'type' : 'password',
        'required' : True
    }))