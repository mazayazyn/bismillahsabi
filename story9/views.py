from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .forms import formAja

# Create your views here.

def loginUser(request):
    if request.method == "POST":
        form = formAja(request.POST)
        if (form.is_valid()):
            userName = form.cleaned_data["user_name"]
            passWord = form.cleaned_data["Password"]
            user = authenticate(request, username = userName, password = passWord)
            if user is not None:
                login(request, user)
        return redirect("/story9/")
    else:
        form = formAja()
        response = {
            "form" : form
        }
        return render(request, "story9/login.html", response)

def registerUser(request):
    if request.method == "POST":
        form = formAja(request.POST)
        if (form.is_valid()):
            userName = form.cleaned_data["user_name"]
            passWord = form.cleaned_data["Password"]
            try:
                user1 = User.objects.get(username = userName)
                return redirect("/story9/register/")
            except User.DoesNotExist:
                user = User.objects.create_user(userName, None, passWord)
                return redirect("/story9/")
    else:
        form = formAja()
        response = {
            "form" : form
        }
        return render(request, "story9/register.html", response)

def logoutUser(request):
    logout(request)
    return redirect("/story9/")