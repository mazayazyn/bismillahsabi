from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve

from .views import loginUser, registerUser, logoutUser

# Create your tests here.
class UnitTest(TestCase):

# Url Test
    def test_url_does_exist(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)
        response2 = self.client.get('/story9/logout/')
        self.assertEqual(response2.status_code, 302)
        response3 = self.client.get('/story9/register/')
        self.assertEqual(response3.status_code, 200)
    
# Template Test
    def test_using_correct_template(self):
        response = self.client.get('/story9/')
        self.assertTemplateUsed(response, 'story9/login.html')
        response3 = self.client.get('/story9/register/')
        self.assertTemplateUsed(response3, 'story9/register.html')
    
# Others
    def test_url_fires_correct_method(self):
        response = resolve('/story9/')
        self.assertEqual(response.func, loginUser)
        response2 = resolve('/story9/logout/')
        self.assertEqual(response2.func, logoutUser)
        response3 = resolve('/story9/register/')
        self.assertEqual(response3.func, registerUser)
    
    def test_trying_login(self):
        response = self.client.post('/story9/', data={'user_name' : 'cantik', 'Password' : 'cantik'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'Halo, cantik')
    
    def test_trying_register_with_existed_account(self):
        response = self.client.post('/story9/register/', data={'user_name' : 'coba', 'Password' : 'coba'})
        html_response = response.content.decode()
        self.assertIn(html_response, 'SELAMAT')
