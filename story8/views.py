from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def bukuCari(request):
    return render(request, 'story8/searchbook.html')

def jsonKumpulan(request):
    try:
        q = request.GET['q']
    except:
        q = "quilting"
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()
    return JsonResponse(json_read)