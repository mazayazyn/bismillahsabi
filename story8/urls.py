from django.urls import path
from .views import bukuCari, jsonKumpulan

urlpatterns = [
    path('', bukuCari),
    path('json/', jsonKumpulan),
]