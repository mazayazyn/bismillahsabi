from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from .views import bukuCari
from selenium.webdriver.chrome.options import Options
import time

class UnitTest(TestCase):

    def test_url_is_exist(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_url_fires_correct_views_method(self):
        reseponse = resolve('/story8/')
        self.assertEqual(reseponse.func, bukuCari)
    
    def test_views_render_correct_html(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'story8/searchbook.html')

    def test_json_Kumpulan(self):
        response = self.client.get('/story8/json/')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()