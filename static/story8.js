$(document).ready(function() {
    $("#search-bar").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        $.ajax({
            // Refers to googleapis in views.py
            url: "json?q=" + q,
            success: function(data) {
                $('#hasil-search').html('')
                var result = '';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" + " </tr>"
                }
                $('#hasil-search').append(result);
            },
            error: function(error) {
                alert("Books not found");
            }
        })
    });
});